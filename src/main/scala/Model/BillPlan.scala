package model

class BillPlan (
	val id: Long,
	val name: String
	) extends Serializable

/** Default bill plan for testing
 */
object DefaultBillPlan extends BillPlan(1, "first")

