package model

class Client (
	val id: Long,
	val name: String
	) extends Serializable

/** Default bill plan for testing
 */
object DefaultClient extends Client(1, "first")

