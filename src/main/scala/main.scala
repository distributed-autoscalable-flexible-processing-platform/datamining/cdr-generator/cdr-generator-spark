import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import com.github.nscala_time.time.Imports._
import spark.Spark.sc
import generator.users._
import generator.operators._
import generator.cells._
import generator.socialnetwork._
import generator.billplans._
import generator.clients._
import simulator._
import model.CDR
import reflect.io._
import Path._
import org.apache.spark.rdd.UnionRDD

object CDRSimulation{

  def regexFileNames(n: String): Option[String] ={
    val regexFiles = """^part-.*"""

    n match {
      case n @ regexFiles => Some(n)
      case _ => None
    }

  }

	def main(args: Array[String]){
		val sim = new BasicSimulator(
			new BasicCellsGenerator(10),
			new HarcodedOperatorsGenerator(),
			new BasicUsersGenerator(50),
			new RandomSocialNetworkGenerator(),
      new HarcodedBillPlansGenerator(),
      new HarcodedClientsGenerator()
		)
    val rdd = sim.simulate(new DateTime)

    rdd.map(_.toString).coalesce(1).saveAsTextFile("../calls")

    scala.tools.nsc.io.File("../calls/header.txt").writeAll(rdd.map(_.header).first)

    //"../calls".toDirectory.files.map(_.name).flatMap(regexFileNames(_))
    new UnionRDD(
      sc,
      Seq(sc.textFile("../calls/header.txt"), sc.textFile("../calls/part-00000"))
    ).coalesce(1).saveAsTextFile("../calls/AllInOne")
	}
}
