package generator.billplans

import model._

abstract class BillPlansGenerator() extends Serializable{
	def generate(): Array[BillPlan]
}

class HarcodedBillPlansGenerator() extends BillPlansGenerator {
	def generate(): Array[BillPlan] = {
		Array(
			new BillPlan(1, "bill plan 1"),
			new BillPlan(2, "bill plan 2")
		)
	}
}
