package generator.clients

import model._

abstract class ClientsGenerator() extends Serializable{
	def generate(): Array[Client]
}

class HarcodedClientsGenerator() extends ClientsGenerator {
	def generate(): Array[Client] = {
		Array(
			new Client(1, "client 1"),
			new Client(2, "client 2")
		)
	}
}
