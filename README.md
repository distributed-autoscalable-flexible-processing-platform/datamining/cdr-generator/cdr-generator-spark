[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3014dd92e20c4e54924271680b6eb122)](https://www.codacy.com/app/samuelbernardo/cdr-generator-spark?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=distributed-autoscalable-flexible-processing-platform/datamining/cdr-generator/cdr-generator-spark&amp;utm_campaign=Badge_Grade)

# CDR generator

The goal is to generate CDR based on different model and to allow model "mix in".  
To achieve that, the generation of the CDR is break down in differents generals steps and for each
steps, there are multiple implementations which respect a common interface. This implies
that we can use any implementation for a step very easily.  

# Data structure

## The events are represented by CDRs

/** Represent a CDR
 * @param fromUser          User who call/SMS67
 * @param toUser            User who recieve the call/SMS
 * @param fromCell          The cell from which fromUser call/SMS
 * @param toCell            The cell from which toUser call/SMS
 * @param date              The date of the call/SMS
 * @param duration          Duration of the call/SMS
 * @param cdrType           Call/SMS
 * @param fromTerminationStatus How the call/SMS terminated (from)
 * @param toTerminationStatus How the call/SMS terminated (to)
 * @param fromValue             Cost of the caller
 * @param toValue             Cost of the reciever
 * @param billingPlan       ID of active billing plan to charge calls
 * @param clientID          ID of client account being charged
 * @param transitType       OnNet/OffNet call/SMS
 * @param fromTac               Device TAC of the caller
 * @param toTac               Device TAC of the receiver
 * @param headerPrint					toString print keys instead of values
 */

## CDR header structure when calling toString

	private def toMap() = {
		Map(
			"FROM_USER" -> fromUser.id.toString,
			"TO_USER" -> toUser.id.toString,
			"FROM_CELL" -> fromCell.id,
			"TO_CELL" -> toCell.id,
			"FROM_OPERATOR" -> fromUser.operator.name,
			"TO_OPERATOR" -> toUser.operator.name,
			"CALL_DURATION" -> duration.toString,
			"TIMESTAMP" -> date.toString("%y%m%d%h%s"),
			"FROM_TERMINATION_STATUS" -> TerminationStatus.toString(fromTerminationStatus),
			"TO_TERMINATION_STATUS" -> TerminationStatus.toString(toTerminationStatus),
			"FROM_VALUE" -> fromValue.toString,
			"TO_VALUE" -> toValue.toString,
			"CALL_TYPE" -> CDRType.toString(cdrType),
			"BILLING_PLAN" -> billingPlan.toString,
			"CLIENT_ID" -> clientID.toString,
			"TRANSIT_TYPE" -> TransitType.toString(transitType),
			"FROM_TAC" -> fromTac,
			"TO_TAC" -> toTac
		)
	}


# Generation Steps
- Generate the cells
- Generate the operator
- Generate the users (operators, cells, schedule, wallet,...)
- Generate the social network
- finally with all those informations use a simulator to simulate one day
    There are multiple simulators possible
    - Iterate over the social graph
    - Go through each id and generate the cdr's according to some distributions
    - Go through each edge of the social graph and generate random cdr for the 
	edge ( **BasicSimulator** )
    - ...

# Usage
To use the generator you must choose the generators you want for each step and
build a **Simulator** with those generators. Then you just need to call simulate 
on the simulator to generate the CDR.

		val simulator = new BasicSimulator(
			new BasicCellsGenerator(10),
			new HarcodedOperatorsGenerator(),
			new BasicUsersGenerator(50),
			new RandomSocialNetworkGenerator()
			)
		simulator.simulate(new DateTime).map(_.toString).saveAsTextFile("test.csv")

To compile and run the test :

	sbt "~test-quick"

To run :

	sbt run

To generate the scala doc (in target scala-../api) :

	sbt doc

# Existing implementation step
**CellsGenerator** :

- **BasicCellsGenerator** : Generate cells randomly in "square".

**OperatorsGenerator**:

- **HardcodedOperatorsGenerator** : Generate 2 hardcoded operators.

**UsersGenerator** : 

- **BasicUsersGenerator** : Generate DumUsers which call from only one cell

**SocialNetworkGenerator**:

- **RandomSocialNetworkGenerator** : Generate a social network based on the logNormalGraph 
    generator the graphx (generate a graph for witch each user has a random 
    (logNormal distribution) number of edges.

**Simulator** :

- **BasicSimulator** : Generate cdr in one pass over the edges of the social graph
   
# Structure :
Source structure :

	Generator/
		- Operator
		- Cells
		- Users
	    - Bill plan
		- Clients
		- Social network
	Simulator/
	IO/
	Model/
	Config
	Main
    
#Next ?
##Streaming mode: 
###Idea 1:
Use a social graph and :

- for each minute compute for each user the call and sms that
    they will made during this minute.
- Update the graph to ensure that there are no one who make 2 phone call simultaneously
- Then collect all the cdr's
- Each second send to the application the cdr's for the second

