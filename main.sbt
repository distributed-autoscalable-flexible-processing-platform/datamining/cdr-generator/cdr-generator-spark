name := "CDR-Generator"

version := "1.0"

//scalaVersion := "2.10.4"
scalaVersion := "2.11.8"
//val sparkVersion = "1.0.0"
val sparkVersion = "1.6.2"
val nscalaTime = "1.2.0"
//val jodaTime = "2.3"
val jodaTime = "2.2.6"
val jodaConvert = "1.2"
//val scalaTest = "2.0"
val scalaTest = "2.2.6"
val sparkStreamingKafka = "2.2.0"
val kafkaConnectCommon = "1.1.0"
val confluentVersion = "4.1.0"

cleanFiles += baseDirectory.value / ".." / "calls"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-graphx" % sparkVersion,
  "com.github.nscala-time" %% "nscala-time" % nscalaTime,
  "joda-time" % "joda-time" % jodaTime,
  "org.joda" % "joda-convert" % jodaConvert,
  "org.scalatest" %% "scalatest" % scalaTest % "test",
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkStreamingKafka,
  "com.datamountaineer" % "kafka-connect-common" % kafkaConnectCommon,
  "io.confluent" % "kafka-connect-avro-converter" % confluentVersion,
  "io.confluent" % "kafka-schema-registry" % confluentVersion
)

scalacOptions in (Compile, doc) ++= Seq("-doc-root-content", baseDirectory.value+"/NOTE.md","-doc-title", "CDR Generator")

resolvers ++= Seq(
  "Akka Repository" at "http://repo.akka.io/release/",
  "Confluent" at "http://packages.confluent.io/maven/",
  "TypeSafe" at "http://repo.typesafe.com/typesafe/releases/"
)

// sbt-assembly
test in assembly := {}

// one-jar
/*import com.github.retronym.SbtOneJar._
oneJarSettings*/
